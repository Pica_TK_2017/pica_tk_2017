﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calmer : MonoBehaviour
{
    public void FixedUpdate()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
