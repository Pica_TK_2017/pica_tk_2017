﻿using UnityEngine;

public class ItemLabel : JamMB
{
    GameObject icon = null;
    float delay = 3.0f;
    bool shouldUpdate = true;

    void Start()
    {
        ItemType itemType = transform.GetComponent<Item>().ItemType;
        icon = Controller.Services.notificationService.ShowIcon(transform.position, itemType);
    }

    void Update()
    {
        if (!shouldUpdate)
            return;
        delay -= Time.deltaTime;
        if (delay < 0.0f)
        {
            if (icon != null)
            {
                Destroy(icon);
                icon = null;
            }

            shouldUpdate = false;
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (icon != null)
            return;
        if (other.tag == "Player")
        {
            ItemType itemType = transform.GetComponent<Item>().ItemType;
            icon = Controller.Services.notificationService.ShowIcon(transform.position, itemType);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(icon);
            icon = null;
        }
    }
}
