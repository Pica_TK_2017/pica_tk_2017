﻿public class Spawner : JamMB
{
    public Item Item;

    public void Start()
    {
        Controller.Services.itemSpawnService.spawners.Add(this);
    }

    public void Clear()
    {
        if (Item != null)
        {
            UnityEngine.GameObject.Destroy(Item.gameObject);
            Item = null;
        }
    }

    public void Set(Item item)
    {
        Item = item;
    }
}
