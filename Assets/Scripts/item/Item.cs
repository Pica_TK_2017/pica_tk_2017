﻿public class Item : JamMB
{
    public ItemType ItemType;

    public void Start()
    {
        Controller.Services.itemSpawnService.items.Add(this);
    }

    public void PickUp()
    {
        Controller.Services.itemSpawnService.items.Remove(this);
    }
}
