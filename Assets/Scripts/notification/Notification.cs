﻿using UnityEngine;
using UnityEngine.UI;

public class DangerNotification : JamMB
{
    CraftTable Target;
    bool isVisible;

    public void SetTarget(CraftTable target)
    {
        Target = target;
        SetSprites();
        Update();
    }

    void Update()
    {
        isVisible = CheckIfIsVisible();
        if (Target != null)
            SetPosition();
        FindChild("Exclamation").gameObject.SetActive(!isVisible);
        FindChild("Icons").gameObject.SetActive(isVisible);
        FindChild("Icons/Plus1").SetActive(Target.Items.Contains(Target.itemsToBeCrafted[0]));
        FindChild("Icons/Plus2").SetActive(Target.Items.Contains(Target.itemsToBeCrafted[1]));
        FindChild("Icons/Plus3").SetActive(Target.Items.Contains(Target.itemsToBeCrafted[2]));
    }

    void SetPosition()
    {
        if (isVisible)
        {
            Vector3 vector = Camera.main.WorldToScreenPoint(Target.transform.position);
            gameObject.transform.position = vector;
        }
        else
        {
            Vector3 screenPoint = Camera.main.WorldToViewportPoint(Target.transform.position);
            screenPoint.x = Mathf.Max(0.04f, Mathf.Min(screenPoint.x, 0.96f));
            screenPoint.y = Mathf.Max(0.03f, Mathf.Min(screenPoint.y, 0.97f));
            gameObject.transform.position = Camera.main.ViewportToScreenPoint(screenPoint);
        }
    }

    bool CheckIfIsVisible()
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(Target.transform.position);
        return screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
    }

    void SetSprites()
    {
        FindChild("Icons/Icon1").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icons/" + Target.itemsToBeCrafted[0].ToString());
        FindChild("Icons/Icon2").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icons/" + Target.itemsToBeCrafted[1].ToString());
        FindChild("Icons/Icon3").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icons/" + Target.itemsToBeCrafted[2].ToString());
    }
}
