﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerNotification : JamMB
{
    Player Target;
    bool isVisible;

    public void SetTarget(Player target)
    {
        Target = target;
        SetSprite();
        Update();
    }

    void Update()
    {
        if (Target != null)
            SetPosition();
    }

    void SetPosition()
    {
        Vector3 vector = Camera.main.WorldToScreenPoint(Target.transform.position);
        gameObject.transform.position = vector;
    }

    void SetSprite()
    {
        FindChild("Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icons/" + Target.holdingItem.ToString());
    }
}
