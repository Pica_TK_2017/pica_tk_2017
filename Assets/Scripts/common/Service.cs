﻿using System;

public interface IService
{
    void Update(float deltatime);
}

public class Service : IService
{
    protected Services services;

    public Service(Services services)
    {
        this.services = services;
    }

    public virtual void Update(float deltatime)
    {

    }
}
