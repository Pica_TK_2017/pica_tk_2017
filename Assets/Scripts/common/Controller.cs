﻿using System.Collections;
using UnityEngine;

public static class MainController
{
    public static IController StaticController { get; set; }
}

public interface IController
{
    Services Services { get; }
}

public class Controller : MonoBehaviour, IController
{
    public Services Services { get; private set; }

    public void Awake()
    {
        MainController.StaticController = this;
        Services = new Services(this);
        Services.InitServices();
    }

    public void Start()
    {
        Debug.Log("start");
        StartCoroutine(ShowMap());
    }

    private void StartGame()
    {
        Services.itemSpawnService.Respawn();
    }

    private IEnumerator ShowMap()
    {
        Debug.Log("Show map");
        Camera mainCamera = Camera.main;
        float time = 0;
        while (time < 1.25f)
        {
            time += Time.deltaTime;
            mainCamera.transform.position = new Vector3(mainCamera.transform.position.x + 0.2f, mainCamera.transform.position.y + 0.4f, mainCamera.transform.position.z);
            yield return null;
        }

        StartGame();
        yield return new WaitForSeconds(4f);

        time = 0;
        while (time < 1.25f)
        {
            time += Time.deltaTime;
            mainCamera.transform.position = new Vector3(mainCamera.transform.position.x - 0.2f, mainCamera.transform.position.y - 0.4f, mainCamera.transform.position.z);
            yield return null;
        }

        LateStartGame();
    }

    public void LateStartGame()
    {
        Services.playersService.EnablePlayers();
        Services.craftService.ShowDanger();

    }

    void Update()
    {
        if (Services != null)
            Services.Update(Time.deltaTime);
    }
}