﻿using UnityEngine;

public class JamMB : MonoBehaviour
{
    private IController controller;
    public IController Controller
    {
        get
        {
            if (controller == null)
                controller = MainController.StaticController;
            return controller;
        }
        set
        {
            controller = value;
        }
    }

    #region Shortcuts

    #endregion

    protected T GetComponentInChild<T>(string path)
    {
        var child = FindChild(path);
        var component = child.GetComponent<T>();
        if (component == null)
            Debug.LogError("No component: " + typeof(T).ToString() + " in child: " + path);

        return component;
    }

    public GameObject FindChild(string path)
    {
        var child = transform.FindChild(path);
        if (child == null)
            Debug.LogError("No child in path: " + path);

        return child.gameObject;
    }
}