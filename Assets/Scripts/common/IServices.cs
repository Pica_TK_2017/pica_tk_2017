﻿using System.Collections.Generic;

public class Services
{
    private List<Service> allServices = new List<Service>();

    public GameService gameService;
    public PlayersService playersService;
    public CraftService craftService;
    public ItemSpawnService itemSpawnService;
    public LayerService layerService;
    public NotificationService notificationService;
    public ItemsProviderService itemsProvider;

    public Services(IController controller)
    {
        gameService = new GameService(this);
        allServices.Add(gameService);

        playersService = new PlayersService(this);
        allServices.Add(playersService);

        craftService = new CraftService(this);
        allServices.Add(craftService);

        itemSpawnService = new ItemSpawnService(this);
        allServices.Add(itemSpawnService);

        layerService = new LayerService(this);
        allServices.Add(layerService);

        notificationService = new NotificationService(this);
        allServices.Add(notificationService);

        itemsProvider = new ItemsProviderService(this);
        allServices.Add(itemsProvider);
    }

    public void InitServices()
    {

    }

    public void Update(float deltatime)
    {
        for (int i = 0; i < allServices.Count; i++)
            allServices[i].Update(deltatime);
    }
}