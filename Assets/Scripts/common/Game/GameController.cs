﻿public class GameController : Service
{
    readonly Services Services;

    public CameraController CameraController { get; private set; }

    public GameController(Services services) : base(services)
    {
        CameraController = new CameraController(Services);
        CameraController.Test();
    }

    public override void Update(float deltatime)
    {
        CameraController.Update(deltatime);
    }

    public void Destroy()
    {
        CameraController.Destroy();
    }
}
