﻿using System.Collections.Generic;
using UnityEngine;

public class CameraController
{
    readonly Services Services;
    readonly List<GameObject> Characters = new List<GameObject>();

    public CameraController(Services services)
    {
        Services = services;
    }

    public void Test()
    {
        GameObject go = new GameObject();
        go.transform.position = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        GameObject goSecond = new GameObject();
        goSecond.transform.position = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        GameObject goThird = new GameObject();
        goThird.transform.position = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        AddCharacter(go);
        AddCharacter(goSecond);
        AddCharacter(goThird);
    }

    public void AddCharacter(GameObject character)
    {
        Characters.Add(character);
    }

    public void Update(float deltatime)
    {
        if (Characters.Count > 0)
        {
            //Vector3 destination = CalculateDestination();
            //destination.z = Camera.main.transform.position.z;
            //Camera.main.transform.position = destination;
        }
    }

    public void Destroy()
    {
        Characters.Clear();
    }

    Vector3 CalculateDestination()
    {
        Vector3 position = new Vector3();
        for (int i = 0; i < Characters.Count; i++)
            position += Characters[i].transform.position;

        return position / Characters.Count;
    }
}
