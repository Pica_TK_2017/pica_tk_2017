﻿public class GameService : Service
{
    GameController GameController;

    public GameService(Services services) : base(services)
    {
        Start();
    }

    public void Start()
    {
        GameController = new GameController(services);
    }

    public override void Update(float deltatime)
    {
        if (GameController != null)
            GameController.Update(deltatime);
    }

    public void End()
    {
        GameController.Destroy();
        GameController = null;
    }
}
