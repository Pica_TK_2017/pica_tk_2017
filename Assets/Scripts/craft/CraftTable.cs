﻿using System.Collections.Generic;

public class CraftTable : JamMB
{
    public int MAX_ITEMS = 3;
    public List<ItemType> Items = new List<ItemType>();
    public List<ItemType> itemsToBeCrafted;

    public bool CanAddItem(ItemType itemType)
    {
        return Items.Count < MAX_ITEMS && itemsToBeCrafted.Contains(itemType) && !Items.Contains(itemType);
    }

    void Start()
    {
        Controller.Services.craftService.tables.Add(this);
    }

    public void AddItem(ItemType item)
    {
        Items.Add(item);
        if (Items.Count == MAX_ITEMS)
            CraftItem();
    }

    void CraftItem()
    {
        Items.Clear();
        Controller.Services.craftService.tables.Add(this);
        itemsToBeCrafted.Clear();
        Controller.Services.craftService.HideDanger(this);
        Controller.Services.craftService.ShowDanger();
    }

    public void SetItems(List<ItemType> list)
    {
        itemsToBeCrafted = list;
    }
}
