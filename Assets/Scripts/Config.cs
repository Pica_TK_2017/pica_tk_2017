﻿public static class GameConfig
{
    public const float ENEMY_DELAY = 60.0f;
    public const float SOUND_DELAY = 10.0f;
}
