﻿using UnityEngine;

public class Player : JamMB
{
    public int index;

    public float movementSpeed = 1;
    public bool input;
    Collider Other;

    private Vector3 movementVector;

    public ItemType holdingItem;

    void Start()
    {
        Controller.Services.playersService.players.Add(this);
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        if (!input)
            return;

        if (Other != null && Input.GetAxis("joy_" + index + "_x") != 0)
            ClickedX(Other);
        ReadInputForXbox360();
    }

    private void ReadInputForXbox360()
    {
        movementVector.x = Input.GetAxis("joy_" + index + "_axis_1") * movementSpeed * 1f;
        movementVector.z = Input.GetAxis("joy_" + index + "_axis_0") * movementSpeed * 1f;

        if (Mathf.Abs(movementVector.x) < 0.1f && Mathf.Abs(movementVector.z) < 0.2f)
        {
            //GetComponentInChild<Animator>("Body").SetBool("walking", false);
            return;
        }

        //GetComponentInChild<Animator>("Body").SetBool("walking", true);

        transform.LookAt(transform.position + movementVector / 10);
        GetComponent<Rigidbody>().MovePosition(transform.position + movementVector / 10);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Item" || other.tag == "Enemy")
            Other = other;

    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Item" || other.tag == "Enemy")
            other = null;
    }

    void ClickedX(Collider other)
    {
        Debug.Log("x clicked on: " + index + " Object: " + other.gameObject.name);

        string tag = other.gameObject.tag;
        if (tag == "Item")
            PickUpItem(other.gameObject);
        else if (tag == "Enemy")
            AddItemToCraftTable(other.gameObject);
    }

    void PickUpItem(GameObject other)
    {
        if (other.GetComponent<Item>() != null)
        {
            holdingItem = other.GetComponent<Item>().ItemType;
            Controller.Services.notificationService.HidePlayerIcon(this);
            Controller.Services.notificationService.ShowPlayerIcon(this);
        }
    }

    void AddItemToCraftTable(GameObject gameObject)
    {
        CraftTable craftTable= gameObject.GetComponent<CraftTable>();
        if (holdingItem != ItemType.None && craftTable.CanAddItem(holdingItem))
        {
            craftTable.AddItem(holdingItem);
            Controller.Services.notificationService.HidePlayerIcon(this);
            holdingItem = ItemType.None;
        }
    }
}