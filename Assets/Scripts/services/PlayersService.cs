﻿using System.Collections.Generic;

public class PlayersService : Service
{
    public List<Player> players = new List<Player>();

    public PlayersService(Services services) : base(services)
    {
    }

    public void DisablePlayers()
    {
        foreach (var player in players)
            player.input = false;
    }

    public void EnablePlayers()
    {
        foreach (var player in players)
            player.input = true;
    }
}
