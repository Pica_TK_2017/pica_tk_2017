﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class NotificationService : Service
{
    public NotificationService(Services services) : base(services)
    {
    }

    public void ShowDanger(CraftTable craftTable)
    {
        GameObject go = UnityEngine.GameObject.Instantiate(Resources.Load<GameObject>("Notifications/Danger"));
        go.AddComponent<DangerNotification>().SetTarget(craftTable);
        go.transform.SetParent(services.layerService.Canvas().transform, false);
        craftTable.gameObject.AddComponent<DangerNotificationHolder>().notification = go.GetComponent<DangerNotification>();
    }

    public void HideDanger(CraftTable table)
    {
        DangerNotificationHolder notificationHolder = table.GetComponent<DangerNotificationHolder>();
        if (notificationHolder != null)
        {
            GameObject.Destroy(notificationHolder.notification.gameObject);
            PlayerNotificationHolder.Destroy(notificationHolder);
        }
    }

    public void ShowPlayerIcon(Player player)
    {
        GameObject go = UnityEngine.GameObject.Instantiate(Resources.Load<GameObject>("Notifications/Player"));
        go.AddComponent<PlayerNotification>().SetTarget(player);
        go.transform.SetParent(services.layerService.Canvas().transform, false);
        player.gameObject.AddComponent<PlayerNotificationHolder>().notification = go.GetComponent<PlayerNotification>();
    }

    public void HidePlayerIcon(Player player)
    {
        PlayerNotificationHolder notificationHolder = player.gameObject.GetComponent<PlayerNotificationHolder>();
        if (notificationHolder != null)
        {
            GameObject.Destroy(notificationHolder.notification.gameObject);
            PlayerNotificationHolder.Destroy(notificationHolder);
        }
    }

    public GameObject ShowIcon(Vector3 position, ItemType icon)
    {
        GameObject go = UnityEngine.GameObject.Instantiate(Resources.Load<GameObject>("Notifications/Player"));
        go.transform.FindChild("Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Icons/" + icon.ToString());
        go.transform.SetParent(services.layerService.Canvas().transform, false);
        go.transform.position = Camera.main.WorldToScreenPoint(position);
        return go;
    }
}
