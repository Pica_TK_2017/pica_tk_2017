﻿using System.Collections.Generic;

public class ItemsProviderService : Service
{
    List<ItemType> itemTypes = new List<ItemType>();

    public ItemsProviderService(Services services) : base(services)
    {
        itemTypes = new List<ItemType> { ItemType.bat, ItemType.chainsaw, ItemType.crowbar, ItemType.dildo,
    ItemType.funnel, ItemType.glue, ItemType.hangar, ItemType.jojo, ItemType.knife, ItemType.laska, ItemType.mask,
    ItemType.razor, ItemType.spray, ItemType.suszarka, ItemType.sznur, ItemType.tape, ItemType.tasak, ItemType.pen,
    ItemType.doll, ItemType.torch, ItemType.fork };
    }

    public List<ItemType> GetItemsToBeCrafted()
    {
        itemTypes.Shuffle();
        return new List<ItemType> { itemTypes[0], itemTypes[1], itemTypes[2] };
    }
}
