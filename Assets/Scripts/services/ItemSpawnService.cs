﻿using System.Collections.Generic;
using UnityEngine;

public class ItemSpawnService : Service
{
    public List<Item> items = new List<Item>();
    public List<Spawner> spawners = new List<Spawner>();

    List<ItemType> itemTypes = new List<ItemType> { ItemType.bat, ItemType.chainsaw, ItemType.crowbar, ItemType.dildo,
    ItemType.funnel, ItemType.glue, ItemType.hangar, ItemType.jojo, ItemType.knife, ItemType.laska, ItemType.mask,
    ItemType.razor, ItemType.spray, ItemType.suszarka, ItemType.sznur, ItemType.tape, ItemType.tasak, ItemType.pen,
    ItemType.doll, ItemType.torch, ItemType.fork};

    public ItemSpawnService(Services services) : base(services)
    {
    }

    public void Respawn()
    {
        Clear();
        spawners.Shuffle();
        for (int i = 0; i < itemTypes.Count; i++)
            Spawn(itemTypes[i], spawners[i]);
    }

    void Clear()
    {
        for (int i = 0; i < spawners.Count; i++)
            spawners[i].Clear();
    }

    void Spawn(ItemType itemType, Spawner spawner)
    {
        GameObject go = GameObject.Instantiate(Resources.Load<GameObject>("Item"));
        go.transform.position = spawner.transform.position;
        go.AddComponent<Item>().ItemType = itemType;
        spawner.Set(go.GetComponent<Item>());
    }
}