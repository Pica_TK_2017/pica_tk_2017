﻿using System.Collections.Generic;
using UnityEngine;

public class CraftService : Service
{
    public List<CraftTable> tables = new List<CraftTable>();
    public List<string> Clips = new List<string> { "bez_rozwoju_jestes_w_dupie", "kto_ci_ukradl_marzenia", "otwieraj", "wpuszczaj_nas", "wspinaj_sie_po_sinusoidzie_sukcesu", "wyjdz_ze_strefy_komfortu", "zadbam_o_twoj_rozwoj", "zadbam_o_twoja_kariere", "knockknock", "knockknock", "knockknock", "knockknock" };
    float soundDelay = GameConfig.SOUND_DELAY;

    public CraftService(Services services) : base(services)
    {
    }

    public override void Update(float deltatime)
    {
        soundDelay -= Time.deltaTime;
        if (soundDelay < 0.0f)
        {
            soundDelay = GameConfig.SOUND_DELAY;
            PlayAudio();
        }
    }

    public void HideDanger(CraftTable table)
    {
        services.notificationService.HideDanger(table);
    }

    public void ShowDanger()
    {
        PlayAudio();
        tables.Shuffle();
        CraftTable table = tables[0];
        table.SetItems(services.itemsProvider.GetItemsToBeCrafted());
        tables.RemoveAt(0);
        services.notificationService.ShowDanger(table);
    }

    public void PlayAudio()
    {
        Clips.Shuffle();
        AudioSource source = UnityEngine.GameObject.Instantiate(Resources.Load<GameObject>("Audio").GetComponent<AudioSource>());
        source.name = "asdasdasasdasdad";
        AudioClip clip = Resources.Load<AudioClip>(Clips[0]);
        source.PlayOneShot(clip);
    }
}
