﻿using UnityEngine;

public class LayerService : Service
{
    Canvas canvas;

    public LayerService(Services services) : base(services)
    {
        canvas = UnityEngine.Canvas.Instantiate<GameObject>(Resources.Load<GameObject>("Canvas")).GetComponent<Canvas>();
    }

    public Canvas Canvas()
    {
        return canvas;
    }
}
